import pytest

from xeneta.config import Settings
from xeneta.core.db import DbRepository, PostgresDbRepository


@pytest.fixture
def settings() -> Settings:
    return Settings()


@pytest.fixture
def db_repository(settings: Settings) -> DbRepository:
    db_repo = PostgresDbRepository.get_instance(settings=settings)
    return db_repo
