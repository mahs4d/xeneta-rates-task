from datetime import date

from fastapi.testclient import TestClient

from xeneta.main import app

client = TestClient(app=app)


def test_api(db_repository):
    response = client.request(method='GET', url='/rates', params={
        'origin': 'CNSGH',
        'destination': 'DKAAR',
        'date_from': date(2016, 1, 1).strftime('%Y-%m-%d'),
        'date_to': date(2016, 1, 4).strftime('%Y-%m-%d'),
    })

    assert response.json() == [
        {
            'day': date(2016, 1, 1).strftime('%Y-%m-%d'),
            'average_price': 1481,
        },
        {
            'day': date(2016, 1, 2).strftime('%Y-%m-%d'),
            'average_price': 1481,
        },
        {
            'day': date(2016, 1, 3).strftime('%Y-%m-%d'),
            'average_price': None,
        },
        {
            'day': date(2016, 1, 4).strftime('%Y-%m-%d'),
            'average_price': None,
        },
    ]
