from datetime import date

import pytest

from xeneta.core.db import DbRepository
from xeneta.rates.entities import DailyAveragePrice
from xeneta.rates.services import RatesServices


@pytest.fixture
def rates_services(db_repository: DbRepository) -> RatesServices:
    return RatesServices(db_repository=db_repository)


def test_port_to_port_average_daily_price(rates_services):
    results = rates_services.get_average_daily_price(
        origin='CNSGH',
        destination='DKAAR',
        date_from=date(2016, 1, 1),
        date_to=date(2016, 1, 4),
    )
    assert results == [
        DailyAveragePrice(
            day=date(2016, 1, 1),
            average_price=1481,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 2),
            average_price=1481,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 3),
            average_price=None,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 4),
            average_price=None,
        ),
    ]


def test_port_to_region_average_daily_price(rates_services):
    results = rates_services.get_average_daily_price(
        origin='CNYAT',
        destination='baltic',
        date_from=date(2016, 1, 1),
        date_to=date(2016, 1, 4),
    )
    assert results == [
        DailyAveragePrice(
            day=date(2016, 1, 1),
            average_price=1545,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 2),
            average_price=1545,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 3),
            average_price=None,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 4),
            average_price=None,
        ),
    ]


def test_region_to_region_average_daily_price(rates_services):
    results = rates_services.get_average_daily_price(
        origin='china_main',
        destination='northern_europe',
        date_from=date(2016, 1, 1),
        date_to=date(2016, 1, 4),
    )
    assert results == [
        DailyAveragePrice(
            day=date(2016, 1, 1),
            average_price=1463,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 2),
            average_price=1463,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 3),
            average_price=None,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 4),
            average_price=None,
        ),
    ]


def test_region_to_port_average_daily_price(rates_services):
    results = rates_services.get_average_daily_price(
        origin='china_main',
        destination='RULED',
        date_from=date(2016, 1, 1),
        date_to=date(2016, 1, 4),
    )
    assert results == [
        DailyAveragePrice(
            day=date(2016, 1, 1),
            average_price=1685,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 2),
            average_price=1685,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 3),
            average_price=None,
        ),
        DailyAveragePrice(
            day=date(2016, 1, 4),
            average_price=None,
        ),
    ]
