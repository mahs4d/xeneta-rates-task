from pydantic import BaseSettings


class Settings(BaseSettings):
    postgres_host: str = 'localhost:5432'
    postgres_user: str = 'ratestask'
    postgres_password: str = 'ratestask'
    postgres_db: str = 'ratestask'

    class Config:
        env_prefix = 'RATESTASK_'
