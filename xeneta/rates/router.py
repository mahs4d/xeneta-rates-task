from datetime import date

from fastapi import APIRouter, Depends

from xeneta.rates import dependencies as rates_dependencies
from xeneta.rates.entities import DailyAveragePrice
from xeneta.rates.services import RatesServices

router = APIRouter()


@router.get('/rates', response_model=list[DailyAveragePrice])
def get_average_daily_prices(
        origin: str,
        destination: str,
        date_from: date,
        date_to: date,
        rates_services: RatesServices = Depends(rates_dependencies.rates_services),
):
    """
    gets average prices per day for specified origin and destination (port or region).
    """

    return rates_services.get_average_daily_price(
        origin=origin,
        destination=destination,
        date_from=date_from,
        date_to=date_to,
    )
