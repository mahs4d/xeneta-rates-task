from fastapi import Depends

from xeneta.core import dependencies as core_dependencies
from xeneta.core.db import DbRepository
from xeneta.rates.services import RatesServices


def rates_services(db_repository: DbRepository = Depends(core_dependencies.db_repository)) -> RatesServices:
    return RatesServices(db_repository=db_repository)
