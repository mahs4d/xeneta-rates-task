from datetime import date

from xeneta.core.db import DbRepository
from xeneta.rates.entities import DailyAveragePrice


class RatesServices:
    def __init__(self, db_repository: DbRepository):
        self.db_repository = db_repository

    def get_average_daily_price(
            self,
            origin: str,
            destination: str,
            date_from: date,
            date_to: date,
    ) -> list[DailyAveragePrice]:
        """
        gets average prices per day for specified origin and destination (port or region).
        skips over days with less than 3 price rows
        """

        query = """
            WITH RECURSIVE orig_query AS (
                SELECT slug FROM regions WHERE slug = :origin
                UNION
                SELECT r.slug FROM regions r
                INNER JOIN orig_query s ON s.slug = r.parent_slug
            ),
            dest_query AS (
                SELECT slug FROM regions WHERE slug = :destination
                UNION
                SELECT r.slug FROM regions r
                INNER JOIN dest_query s ON s.slug = r.parent_slug
            ),
            all_days AS (
                SELECT date_trunc('day', generate_series)::date AS day FROM
                generate_series(:date_from ::timestamp, :date_to ::timestamp, '1 day'::interval)
            )
            SELECT day, p.average_price from all_days
            LEFT JOIN (
                SELECT day, ROUND(AVG(price)) AS average_price, COUNT(*) AS price_count FROM prices
                INNER JOIN ports orig_port ON prices.orig_code = orig_port.code
                INNER JOIN ports dest_port ON prices.dest_code = dest_port.code
                WHERE (prices.orig_code = :origin OR orig_port.parent_slug IN (SELECT slug FROM orig_query)) AND
                      (prices.dest_code = :destination OR dest_port.parent_slug IN (SELECT slug FROM dest_query)) AND
                      (prices.day >= :date_from AND prices.day <= :date_to)
                GROUP BY day
                HAVING COUNT(*) >= 3
                ORDER BY day
            ) p USING (day);
        """

        # run query
        db_rows = self.db_repository.run_query(
            sql_query=query,
            params={
                'origin': origin,
                'destination': destination,
                'date_from': date_from.strftime('%Y-%m-%d'),
                'date_to': date_to.strftime('%Y-%m-%d'),
            }
        )

        # convert queries to pydantic entities
        return [DailyAveragePrice(
            day=db_row[0],
            average_price=int(db_row[1]) if db_row[1] is not None else None,
        ) for db_row in db_rows]
