from datetime import date

from pydantic import BaseModel


class DailyAveragePrice(BaseModel):
    day: date
    average_price: int | None
