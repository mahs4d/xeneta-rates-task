from xeneta.core.api import Api

# create an instance fastapi app for uvicorn
app = Api().get_fastapi_app()
