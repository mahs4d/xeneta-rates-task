from __future__ import annotations

from abc import ABC, abstractmethod

from sqlalchemy.engine import create_engine
from sqlalchemy.sql import text

from xeneta.config import Settings


class DbRepository(ABC):
    @abstractmethod
    def run_query(self, sql_query: str, params: dict):
        """
        run a query and return the result
        """
        pass


class PostgresDbRepository(DbRepository):
    """
    DbRepository implementation, using sqlalchemy to communicate to postgres
    """
    
    _instance: PostgresDbRepository | None = None

    def __init__(
            self,
            postgres_host: str,
            postgres_user: str,
            postgres_password: str,
            postgres_db: str,
    ):
        if PostgresDbRepository._instance:
            raise Exception('use get_instance() method')

        self.engine = create_engine(
            url=f'postgresql://{postgres_user}:{postgres_password}@{postgres_host}/{postgres_db}',
            future=True,
        )

        super().__init__()

    def run_query(self, sql_query: str, params: dict):
        with self.engine.connect() as con:
            return con.execute(text(sql_query), params)

    @classmethod
    def get_instance(cls, settings: Settings) -> PostgresDbRepository:
        # this class should be singleton, so this function should be used everywhere instead of constructor

        if not cls._instance:
            cls._instance = cls(
                postgres_host=settings.postgres_host,
                postgres_user=settings.postgres_user,
                postgres_password=settings.postgres_password,
                postgres_db=settings.postgres_db,
            )

        return cls._instance
