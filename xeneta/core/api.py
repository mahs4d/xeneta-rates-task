from fastapi import FastAPI

from xeneta.rates.router import router as rates_router


class Api:
    def get_fastapi_app(self):
        app = FastAPI()

        app.include_router(router=rates_router)

        return app
