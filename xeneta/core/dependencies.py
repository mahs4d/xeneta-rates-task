from fastapi import Depends

from xeneta.config import Settings
from xeneta.core.db import DbRepository, PostgresDbRepository


def settings() -> Settings:
    return Settings()


def db_repository(settings: Settings = Depends(settings)) -> DbRepository:
    return PostgresDbRepository.get_instance(settings=settings)
