# Xeneta Rates Task

the task was implemented using `fastapi` and raw sql over postgres.

### How to run:
please install dependencies using:

`poetry install`

then you can start the server with:

`uvicorn xeneta.main:app`
