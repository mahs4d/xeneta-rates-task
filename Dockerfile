FROM  postgres:15-alpine

COPY migrations/* /docker-entrypoint-initdb.d/

EXPOSE 5432

ENV POSTGRES_USER=ratestask
ENV POSTGRES_PASSWORD=ratestask
ENV POSTGRES_DB=ratestask
